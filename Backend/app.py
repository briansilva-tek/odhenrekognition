from flask import Flask, jsonify, request, Response
from flask_cors import CORS
from classes.camera import VideoCamera
from classes.ws import WebSocket
from classes.PessoasController import PessoasController
from classes.CollectionController import CollectionController
import os

app = Flask(__name__)
CORS(app, resources={r'/*': {'origins': '*'}})

ws = WebSocket()
ws.main()
camera = VideoCamera(ws)
pessoas = PessoasController()
collection = CollectionController()

print("  ____  _____  _    _ ______ _   _     _____  ____  ")
print(" / __ \|  __ \| |  | |  ____| \ | |   / ____|/ __ \ ")
print("| |  | | |  | | |__| | |__  |  \| |  | |  __| |  | |")
print("| |  | | |  | |  __  |  __| | . ` |  | | |_ | |  | |")
print("| |__| | |__| | |  | | |____| |\  |  | |__| | |__| |")
print(" \____/|_____/|_|  |_|______|_| \_|   \_____|\____/ ")
print("\n\n")
                                                    
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

@app.route('/cadastro/pessoa/capturarfoto', methods=['POST'])
def cadastroPessoaCapturarFoto():

    message = ""
    status = True

    try:
        id = request.json.get('id')

        if(id == None):
            raise Exception("ID do usuário não enviado. Parâmetro: id")

        path, label = pessoas.armazenarFoto(id, camera.image)

        resultado = collection.cadastrarFotos(path, label, id)

        if not resultado:
            raise Exception("Ocorreu um erro ao enviar a foto para AWS")


        message = "Imagem salva com sucesso."
    except Exception as e:
        message = str(e)
        status = False

    return jsonify(
        status=status,
        message=message
    )


@app.route('/cadastro/pessoa/salvarnome', methods=['POST'])
def cadastroPessoaGetId():

    message = ""
    status = True
    nome = ""

    
    try:

        nome = request.json.get('nome')

        if(nome == None):
            raise Exception("Nome do usuário não enviado. Parâmetro: nome")        

        id = pessoas.gerarNovoId()
        cpf = ""
        pessoas.armazenarNome(id, nome, cpf)

        message = "Nome salvo com sucesso."
    except Exception as e:
        message = str(e)
        status = False

    return jsonify(
        novoId=id,
        status=status,
        message=message
    )

@app.route('/video_feed', methods=['GET'])
def video_feed():
    return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/collections', methods=['GET'])
def getCollections():
    status = True

    try:

        collections = collection.listarCollection()

    except:
        status = False

    return jsonify(
        status = status,
        collections = collections
    )
    

@app.route('/cadastro/collection', methods=['POST'])
def cadastroCollection():
    
    message = ""
    status = True

    try:

        nome = request.json.get('nome')

        if(nome == None):
            raise Exception("Nome da collection não enviado. Parâmetro: nome")        

        collection.criarCollection(nome)

        message = "Collection criada com sucesso."
    except Exception as e:
        message = str(e)
        status = False

    return jsonify(
        status=status,
        message=message
    )


@app.route('/delete/collection', methods=['POST'])
def deleteCollection():
    
    message = ""
    status = True
    collections = []

    try:

        nome = request.json.get('nome')

        if(nome == None):
            raise Exception("Nome da collection não enviado. Parâmetro: nome")        

        collection.apagarCollection(nome)

        message = "Collection apagada com sucesso."
        collections = collection.listarCollection()
    except Exception as e:
        message = str(e)
        status = False

    return jsonify(
        status=status,
        message=message,
        collections = collections
    )

def gen():
    global camera

    while True:
        camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + camera.imagemJpeg + b'\r\n\r\n')
             
app.run(host="0.0.0.0")