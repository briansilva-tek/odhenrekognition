import boto3
from botocore.config import Config
from decouple import config
import json

AWS_ACCESS_KEY = config('AWS_ACCESS_KEY')
AWS_SECRET_ACCESS_KEY = config('AWS_SECRET_ACCESS_KEY')
REGION_NAME = config('REGION_NAME')
COLLECTION_ID = config('COLLECTION_ID')

CADASTRO_PESSOAS_PATH = config('CADASTRO_PESSOAS_PATH')

class CollectionController:

    def __init__(self):
        self.client = boto3.client('rekognition',
            aws_access_key_id=AWS_ACCESS_KEY,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
            region_name=REGION_NAME,)

        # config=Config(proxies={'http': 'briansilva:teknisa2@192.168.122.121:3128'})
        
    def listarCollection(self):
        maxResults = 15
        client = boto3.client('rekognition',
            aws_access_key_id=AWS_ACCESS_KEY,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
            region_name=REGION_NAME,)
        
        response=client.list_collections(MaxResults=maxResults)
        
        return response

    def criarCollection(self, collectionId):
        response = self.client.create_collection(CollectionId = collectionId)

        return response

    def apagarCollection(self, collectionId):
        response = self.client.delete_collection(CollectionId=collectionId)

        return response

    def cadastrarFotos(self, path, label, id):
        status = True
        id = str(id)
        collectionId = COLLECTION_ID

        try: 
            imgfile = open(path, 'rb')
            imgbytes = imgfile.read()
            imgfile.close()
            
            response = self.client.index_faces(CollectionId=collectionId,
                                    Image={'Bytes': imgbytes },
                                    ExternalImageId=label,
                                    MaxFaces = 1,
                                    QualityFilter="AUTO",
                                    DetectionAttributes=['ALL'])

            with open(CADASTRO_PESSOAS_PATH + '/' + id + '/' + label + ".json", 'w') as f:
                json.dump(response, f)            

        except Exception as e:      
            status = False                                                                  
            print(str(e))
        
        return status

    def listarFaces(self):

        maxResults=2
        tokens=True
        collectionId = COLLECTION_ID
        client = self.client

        response = client.list_faces(CollectionId=collectionId,
                               MaxResults=maxResults)

        while tokens:

            faces=response['Faces']

            for face in faces:
                print (face)
            if 'NextToken' in response:
                nextToken=response['NextToken']
                response=client.list_faces(CollectionId=collectionId,
                                        NextToken=nextToken,MaxResults=maxResults)
            else:
                tokens=False


    def main(self):    
        pass