from websocket_server import WebsocketServer
import threading

class WebSocket():

    def __init__(self):
        self.server = WebsocketServer(13254, host='0.0.0.0')

    def enviarMensagem(self, mensagem):
        """arq = open('Exemplo.json', 'r')
        mensagem = arq.read()
        arq.close()"""
        self.server.send_message_to_all(mensagem)

    def startWS(self):
        print(" [*] Iniciando WS")
        self.server.run_forever()

    def main(self):    
        t1 = threading.Thread(target=self.startWS, daemon=False)
        t1.start()