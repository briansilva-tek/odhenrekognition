import os
from decouple import config
import cv2
import uuid
import json
from datetime import datetime

CADASTRO_PESSOAS_PATH = config('CADASTRO_PESSOAS_PATH')

class PessoasController:

    def __init__(self):
        pass

    def armazenarNome(self, id, nome, cpf):

        pathPasta = CADASTRO_PESSOAS_PATH+'/'+str(id)
        
        if not os.path.exists(pathPasta):
            os.makedirs(pathPasta)

        data = {}
        data['id']   = int(id)
        data['nome'] = nome
        data['cpf'] = cpf
        data['ultimaVisita'] = ""

        with open(pathPasta+'/dados.json', 'w') as outfile:
            json.dump(data, outfile)

    def armazenarFoto(self, id, image):

        id = str(id)

        pathPasta = CADASTRO_PESSOAS_PATH+'/'+id
        nomeImagem = id+'-'+uuid.uuid4().hex+'.jpg'
        fullPath = pathPasta+'/'+nomeImagem

        cv2.imwrite(fullPath, image)

        return (fullPath, nomeImagem)

    def findPessoa(self, id):

        pathPasta = CADASTRO_PESSOAS_PATH+'/'+str(id)
        objRetorno = {}
        objRetorno['status'] = True
        objRetorno['pessoa'] = {}

        if os.path.exists(pathPasta):
            with open(pathPasta+'/dados.json', 'r') as f:
                data = json.load(f)
                objRetorno['pessoa'] = data
        else:
            objRetorno['status'] = False
            objRetorno['pessoa']['nome'] = "NAO ENCONTRADO"
            objRetorno['pessoa']['id'] = id
            objRetorno['pessoa']['ultimaVisita'] = ""

        return objRetorno

    def registrarVisita(self, id):
        pathPasta = CADASTRO_PESSOAS_PATH+'/'+str(id)
        data = {}

        if os.path.exists(pathPasta):
            with open(pathPasta+'/dados.json', 'r') as f:
                data = json.load(f)
                
            now = datetime.now()
                        
            date_time = now.strftime("%d/%m/%Y, %H:%M:%S")
            data['ultimaVisita'] = date_time

            with open(pathPasta+'/dados.json', 'w') as outfile:
                json.dump(data, outfile)


    def gerarNovoId(self):

        files = folders = 0

        for _, dirnames, filenames in os.walk(CADASTRO_PESSOAS_PATH):
            # ^ this idiom means "we won't be using this value"
            files += len(filenames)
            folders += len(dirnames)

        return folders + 1