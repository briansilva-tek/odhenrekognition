import sys
import boto3
import cv2
from decouple import config
import threading
import time
import os
import json
from .PessoasController import PessoasController
import uuid

AWS_ACCESS_KEY = config('AWS_ACCESS_KEY')
AWS_SECRET_ACCESS_KEY = config('AWS_SECRET_ACCESS_KEY')
REGION_NAME = config('REGION_NAME')
COLLECTION_ID = config('COLLECTION_ID')
LOG_ATIVO=config('LOG_ATIVO')
LOG_PATH=config('LOG_PATH')
IMAGE_NAME = "tmpimg.jpg"
FLAG_QUEUE = 1
PATH_TMP_IMGS = 'tmpimgs/'

class DetectController:

    def __init__(self, camera, ws):
        self.camera = camera
        self.websocket = ws
        pass

    def iniciarProcessamento(self):    
        t1 = threading.Thread(target=self.processarCamera, daemon=True )
        t1.start()

    def processarCamera():
        buffer_inp = list()

        elapsed = int()
        start = timer()

        while self.camera.video.isOpened():        
            elapsed += 1
            frame = self.camera.image

            if frame is None:
                print ('\nEnd of Video')
                break

            buffer_inp.append(frame)
            
            # Only process and imshow when queue is full
            if elapsed % FLAG_QUEUE == 0:            
                self.processar()
                # Clear Buffers
                buffer_inp = list()

    def processar(self):

        status = True
        tempoAtual = time.time()
        realizouReconhecimento = False
        possuiFace = self.camera.possuiFace

        #  or ((tempoAtual - ultimoMomentoSemFace) > 5 and possuiFace and (tempoAtual - ultimaRequisicao) > 5)
        if ((tempoAtual - ultimaRequisicao) > 5 and self.camera.possuiFace):
            ultimaRequisicao = tempoAtual
            print(" # Detectando pessoa")
            response = ''

            try:
                cv2.imwrite(IMAGE_NAME, imagemOriginal)
                resultadoDeteccao, response = self.detectar()

                if (resultadoDeteccao):
    
                    externalString = response['FaceMatches'][0]['Face']['ExternalImageId']
                    pos = externalString.find('-')

                    if (pos != -1):
                        pessoa = externalString[:pos]
                        params = {'NRCPFFUNC': pessoa}
                        r = requests.post(API_URL+"pessoa", params)
                        r = r.json()

                        if (r['STATUS'] == True):
                            nomeCompleto = r['NMFUNC'].rstrip().split(' ')
                            pessoa = nomeCompleto[0] + " " + nomeCompleto[len(nomeCompleto)-1]

                    else:
                        pessoa = externalString
                    
                    realizouReconhecimento = True

                else:
                    raise Exception("Ocorreu um erro na detecção")

            except Exception as e:
                status = False
                print(str(e))

    def detectarPessoa(self):    
        t1 = threading.Thread(target=self.detectar, daemon=True )
        t1.start()

    def detectar(self):
        
        threshold = 98
        maxFaces=50

        pessoas = []
        response = []
        status = True

        client = boto3.client(
            'rekognition',
            aws_access_key_id=AWS_ACCESS_KEY,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
            region_name=REGION_NAME,)

        with os.scandir(PATH_TMP_IMGS) as entries:
            for entry in entries:
                try:                                    
                    imgfile = open(entry.path, 'rb')
                    imgbytes = imgfile.read()
                    imgfile.close()
                            
                    imgobj = {'Bytes': imgbytes}

                    result = client.search_faces_by_image(Image=imgobj, CollectionId=COLLECTION_ID, FaceMatchThreshold=threshold, MaxFaces=maxFaces)
                    
                    faceDetails = client.detect_faces(Image=imgobj, Attributes=['ALL'])
                    result['FaceDetails'] = faceDetails

                    if (len(result['FaceMatches']) > 0):
                        externalString = result['FaceMatches'][0]['Face']['ExternalImageId']
                        pos = externalString.find('-')

                        if (pos != -1):
                            idPessoa = externalString[:pos]
                            pessoasController = PessoasController()
                            pessoa = pessoasController.findPessoa(idPessoa)
                            pessoasController.registrarVisita(idPessoa)
                            pessoas.append(pessoa)
                            result['idPessoa'] = idPessoa
                            result['dadosPessoa'] = pessoa
                        else:
                            pessoa = externalString

                    response.append(result)

                    if(self.str_to_bool(LOG_ATIVO)):
                        logFullPath = LOG_PATH+"/"+idPessoa+"-"+uuid.uuid4().hex
                        img = cv2.imread(entry.path, cv2.COLOR_BGR2RGB)
                        cv2.imwrite(logFullPath+".jpg", img)

                        with open(logFullPath+".json", 'w') as f:
                            json.dump(result, f)  

                except Exception as e:
                    print(str(e))
                    status = False
                    break

        resultJson = {
            "status": status,
            "pessoas": pessoas,
            "data": response
        }

        self.websocket.enviarMensagem(json.dumps(resultJson))

        return status, response
    
    def str_to_bool(self, s):
        if s == 'True':
            return True
        elif s == 'False':
            return False
        
            