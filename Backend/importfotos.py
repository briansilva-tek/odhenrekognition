from classes.CollectionController import CollectionController
from classes.PessoasController import PessoasController
import os
import requests
from shutil import copyfile
import cv2

clientcpfs = {}
clientids = {}
files = []
path = '/home/GRUPOTEKNISA/briansilva/Documentos/odhen/Projeto Catraca/aws-rekognition/imagens'
API_URL = "http://briansilva.zeedhi.com/workfolder/apiOdhenRekognition/index.php/"

class ImportarFotos:
    
    def __init__(self):
        pass

    def getNomePessoa(self, pessoa):

        nomePessoa = ""

        if (pessoa not in clientcpfs.keys()):
            params = {'NRCPFFUNC': pessoa}
            r = requests.post(API_URL+"pessoa", params)
            r = r.json()

            if (r['STATUS'] == True):
                clientcpfs[pessoa] = r['NMFUNC']
            
        if(pessoa in clientcpfs.keys()):
            nomePessoa = clientcpfs[pessoa]
        else:
            print("FUNCIONARIO_NAO_CADASTRADO: " + cpf)

        return nomePessoa
    
    def registrarLogCpf(self, cpf):
        with open("logcpfsnaocadastrados.txt", "a") as myfile:
            myfile.write(cpf+"\n")

importar = ImportarFotos()
pessoas = PessoasController()
collection = CollectionController()

for r, d, f in os.walk(path):
    for file in f:
        if '.jpg' in file:
            files.append(file)

for f in files:
    pos = f.find('-')

    if(pos != -1):
        cpf = f[:pos]
        nomePessoa = importar.getNomePessoa(cpf)


        if (cpf not in clientids.keys()):
            id = pessoas.gerarNovoId()
            pessoas.armazenarNome(id, nomePessoa, cpf)
            clientids[cpf] = id
        else:
            id = clientids[cpf]

        if (nomePessoa == ""):
            importar.registrarLogCpf("ID: " + str(id) + " CPF: " + cpf)
        else:
            print(nomePessoa)

        fullPathImg = path+'/'+f
        
        img = cv2.imread(fullPathImg, cv2.COLOR_BGR2RGB)
        pathSalvo, label = pessoas.armazenarFoto(id, img)

        resultado = collection.cadastrarFotos(pathSalvo, label, id)
    
